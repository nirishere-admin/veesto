            var fallingStates = {
              stable:{next:"falling"},
              falling:{next: "hit", back:"stable"}, 
              hit: {next: "fallen", back:"stable"},
              fallen: {next:"stable"}
            };

            var currentState = "stable";
            var fallingState = fallingStates[currentState];
            var screenWidth, screenHeight;
            var xPos, yPos;
            var xSpeed = 1, ySpeed = 1;
            var circleRadius = 50;
            var mainMessage;
            var prevTime = new Date();
            var prevCrashTime = new Date();
            var fallingTime, crashingTime;

            function init() {

                screenWidth = $(window).width();
                screenHeight = $(window).height();
                xPos = screenWidth/2;
                yPos = screenHeight/2;
                mainMessage = document.getElementById("boom");
                document.getElementById("movingCircle").style.width = circleRadius + "px";
                document.getElementById("movingCircle").style.height = circleRadius + "px";
                
                mainMessage.top = xPos;
                mainMessage.left = yPos;
                
                topR = document.getElementById("topR");
                topL = document.getElementById("topL");
                bottomR = document.getElementById("bottomR");
                bottomL = document.getElementById("bottomL");
                
                var objectsToTouch = [topR, topL, bottomR, bottomL];

                if(window.DeviceMotionEvent) {
                window.addEventListener('devicemotion', function(event) {
                            var x = event.accelerationIncludingGravity.x;
                            var y = event.accelerationIncludingGravity.y;
                            var z = event.accelerationIncludingGravity.z;
                        
                            fallingStateCheck(z);
                            
                            xPos -= x * xSpeed;
                            yPos += y * ySpeed;

                            //stay in bounds
                            if(xPos<0)xPos=0;
                            if(xPos>screenWidth)xPos=screenWidth;
                            if(yPos<0)yPos=0;
                            if(yPos>screenHeight)yPos=screenHeight;

                            //translate
                            translateMainObject(xPos ,yPos);
                            objectsToTouch.forEach(element => {
                            if(detectCollision(xPos, yPos, element))
                            {element.style.color = "red"}  
                            });
                            
                        });
                }
            }   

            function switchState(newState){
              currentState = fallingState[newState];
              fallingState = fallingStates[currentState];
            }

            function fallingStateCheck(z){
              var Now = new Date();
              var deltaTime = Now - prevTime;
              var crashTime = Now - prevCrashTime;

              prevCrashTime = Now;
              prevTime = Now;

              switch(currentState){
                case "stable":
                  fallingTime = 0;
                  document.body.style.background = "white";
                  if(z<=4){
                    switchState("next");
                  }
                break;
                case "falling":
                  fallingTime+=deltaTime;
                  if((z<=4) && fallingTime > 200){
                    crashingTime = 0;
                    fallingTime = 0;
                    switchState("next");
                  }else if(z>4){
                    fallingTime = 0;
                    switchState("back");
                  }
                break;
                case "hit":
                  crashingTime+=crashTime;
                  if(z>=9.0){
                      if(crashingTime>=200){
                        crashingTime=0;
                        switchState("back");
                      }else{
                        crashingTime=0;
                        document.body.style.background = "red";
                        mainMessage.style.display = "block";
                        switchState("next");
                      }
                  }

                break;
                case "fallen":
                  switchState("next");
                  fallingTime = 0;
                  setTimeout(function(){
                    document.body.style.background = "white";
                    mainMessage.style.display = "none";
                    },3000);
              }
            }

            function detectCollision(posx, posy, obj){
              dist = Math.sqrt(Math.pow(obj.offsetLeft + obj.clientWidth / 2 - posx,2) + Math.pow(obj.offsetTop + obj.clientHeight / 2 - posy,2));
              if(dist<circleRadius)
                obj.style.backgroundColor = "red";
            }

            function translateMainObject(x,y){
                var elem = document.getElementById("movingCircle");   
                elem.style.top = y + 'px'; 
                elem.style.left = x + 'px';
            }
